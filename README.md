# Ukraine-Russia by data

## Description
Understand UA-RU conflict by data

Dashboard to visualise the data: https://war.excaldata.dev/

## Contributing
Volunteer are very welcome to add new entries or cross check the data quality.

You can simply append a new record following the JSON schema below. Please don't forget to source your info.
Then pen an issue or a merge request if you want to add/modify data to the main branch. 

## Warning
The data are not exact, and can contains multiple errors due, either from the source or during the capture (rank can be infered from picture and not the exact last one). Field like Origine can have different meaning but is in priority the place of birth, schooling, graduation, living place or burial place. 

## Schema
Here is the current Schema used to store the data. 
```
{
    "First_Name": "",
    "Patronymic_name":"",
    "Last_Name": "",
    "First_Name_RU": "",
    "Patronymic_name_RU":"",
    "Last_Name_RU": "",
    "Unit": "",
    "Unit_type": "",
    "Rank": "",
    "Status": "D",
    "Date_of_birth": "",
    "Age": "",
    "Origin": "",
    "Origin.lat":"",
    "Origin.long":"",
    "Place_of_death": "",
    "Death_date": "",
    "Funeral_date":"", 
    "Approx_death_date": "",
    "Earliest_date_of_publication": "",
    "Note":"",
    "Source":[{"link1": ""},
            {"link2": ""},
            {"link3":""}]  
}
```

## Authors and acknowledgment
Initial set of data based on Rob Lee and Necromancer (@666_mancer) Twitter thread 


## Links
* https://zona.media/translate/2022/05/20/casualties_eng (no publication of raw data)
* https://twitter.com/RALee85/status/1528501442415316992 (Rob Lee thread)

## License
CC-BY-SA